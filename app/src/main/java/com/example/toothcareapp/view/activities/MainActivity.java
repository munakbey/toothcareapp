package com.example.toothcareapp.view.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.example.toothcareapp.R;
import com.example.toothcareapp.common.Consts;
import com.example.toothcareapp.presenter.classes.P_UserLoginInfo;
import com.example.toothcareapp.view.fragments.F_DoctorMain;
import com.example.toothcareapp.view.fragments.F_PatientMain;
import com.example.toothcareapp.view.fragments.F_UserLogin;
import com.example.toothcareapp.view.fragments.F_UserRegister;

public class MainActivity extends AppCompatActivity {

    boolean savedLoginStatus;
    P_UserLoginInfo p_userLoginInfo=new P_UserLoginInfo();
    public Fragment mfragment;
    MainActivity mContext;
    public static SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = this.getPreferences(Context.MODE_PRIVATE);
        Log.e("control",savedLoginStatus+"+++");
        savedLoginStatus = sharedPreferences.getBoolean("isLoggedIn",false);


        if(sharedPreferences.getBoolean("isLoggedIn",false)==false){
            Fragment fragment = new F_UserRegister(this);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_frame_layout, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
            Log.e("control","Giris yapilmamis");
            SharedPreferences.Editor prefsEdit = sharedPreferences.edit();
            prefsEdit.putBoolean("isLoggedIn", true);
            prefsEdit.commit();
        }else{
            SharedPreferences mPrefs = getSharedPreferences("mSharedPref",0);
            Log.e("control","Giris YAPİLMİS ");
            if(mPrefs.getString("token","")!=""){
                Log.e("control","Daha önce basarılı giris yapilmis");
                String username=mPrefs.getString("username","");
                String userType=p_userLoginInfo.getUser(username).get(0/*p_userLoginInfo.getUser(username).size()-1*/).getUserType();
                Log.e("control",username+userType);

                if (userType.equals(Consts.USER_PATIENT)) {
                    mfragment = new F_PatientMain(mContext);
                }
                if (userType.equals(Consts.USER_DOCTOR)) {
                    mfragment = new F_DoctorMain(mContext);
                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_frame_layout, mfragment, mfragment.getClass().getSimpleName()).addToBackStack(null).commit();
            }else{
                Fragment fragment = new F_UserLogin(mContext);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_frame_layout, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
            }
        }

 /*       if(savedLoginStatus==false){
            Fragment fragment = new F_UserRegister(this);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_frame_layout, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

// Fragment fragment = new F_UserLogin(this,sharedPreferences);getSupportFragmentManager().beginTransaction().replace(R.id.main_frame_layout, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
        }else{
           String uname  = F_UserLogin.sharedPreferences.getString("isUserName","test");
            Log.e("control",uname+"_____");
            String userType=p_userLoginInfo.getUser("g").get(p_userLoginInfo.getUser("g").size()-1).getUserType();
                if(userType.equals(Consts.USER_PATIENT)){
                    mfragment = new F_PatientMain(mContext);
                }
                if(userType.equals(Consts.USER_DOCTOR)){
                    mfragment = new F_DoctorMain(mContext);
                }

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_frame_layout, mfragment, mfragment.getClass().getSimpleName()).addToBackStack(null).commit();

        }*/


    }


}
