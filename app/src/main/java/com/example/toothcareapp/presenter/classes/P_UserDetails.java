package com.example.toothcareapp.presenter.classes;

import com.example.toothcareapp.common.MyApp;
import com.example.toothcareapp.model.dao.UserDetailsModel;
import com.example.toothcareapp.view.activities.UserDetails;

import java.util.List;

public class P_UserDetails {

    public void addUserDetails(UserDetailsModel userDetailsModel){
        MyApp.userDetailsExecute().addUserDetails(userDetailsModel);
    }
    public List<UserDetailsModel> userDetailsList(){
        return  MyApp.userDetailsExecute().listUserDetails();
    }
    public List<UserDetailsModel> getuserDetailsById(int id){
        return  MyApp.userDetailsExecute().listUserDetailsById(id);
    }
}
