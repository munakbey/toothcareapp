package com.example.toothcareapp.view.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.toothcareapp.R;
import com.example.toothcareapp.common.Consts;
import com.example.toothcareapp.common.Globals;
import com.example.toothcareapp.presenter.classes.P_UserLoginInfo;

public class F_UserLogin extends Fragment {
    private Context mContext;
    private View rootView;
    P_UserLoginInfo p_userLoginInfo=new P_UserLoginInfo();
    public  static SharedPreferences sharedPref;
    boolean savedLoginStatus;
    public static Fragment mfragment;
    TextView txtUsername,txtPassword;
    EditText input;
    AlertDialog mAlarmDialog;
    public SharedPreferences mShrPrf;
    int lastSavedUserId;

    @SuppressLint("ValidFragment")
    public F_UserLogin(Context mContext/*, SharedPreferences sharedPreferences*/) {
        this.mContext = mContext;
       // this.sharedPref=sharedPreferences;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view=inflater.inflate(R.layout.fragment_f__user_login,container,false);
        txtPassword=view.findViewById(R.id.txt_password);
        txtUsername=view.findViewById(R.id.txt_user_name_);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sharedPref = getActivity().getSharedPreferences("mSharedPref",0);
        savedLoginStatus = sharedPref.getBoolean("isLoggedIn",false);


        view.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtPassword.getText().toString().equals(p_userLoginInfo.getUser(txtUsername.getText().toString()).get(0).getPassword())) {

                    SharedPreferences.Editor prefsEdit = sharedPref.edit();
                    prefsEdit.putString("username", txtUsername.getText().toString());
                    prefsEdit.commit();

                    String userType=p_userLoginInfo.getUser(txtUsername.getText().toString()).get(0/*p_userLoginInfo.getUser(username).size()-1*/).getUserType();
                    Log.e("control",p_userLoginInfo.getUser("test").size()+" =size"+txtPassword.getText().toString()+" ****** "+
                            txtUsername.getText().toString()+" ! "+userType+" "+savedLoginStatus);

                        if (userType.equals(Consts.USER_PATIENT)) {
                            mfragment = new F_PatientMain(mContext);
                        }
                        if (userType.equals(Consts.USER_DOCTOR)) {
                            mfragment = new F_DoctorMain(mContext);
                        }

                codeDialog();
                mAlarmDialog.show();
             /*   getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_frame_layout, mfragment, mfragment.getClass().getSimpleName()).addToBackStack(null).commit();*/
                }
            }
        });
    }

    public void codeDialog(){
        AlertDialog.Builder builder=new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.code);
        builder.setMessage(R.string.enter_code);

        input=new EditText(mContext);
        builder.setView(input);

        builder.setPositiveButton(R.string.submit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String txt=input.getText().toString();
                    if(txt.equals(txt)){//CHECHK SMS CODE
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.main_frame_layout, mfragment, mfragment.getClass().getSimpleName()).addToBackStack(null).commit();

                        lastSavedUserId=p_userLoginInfo.getAllUser().get(p_userLoginInfo.getAllUser().size()-1).getId();

                        SharedPreferences.Editor prefsEdit = sharedPref.edit();
                        prefsEdit.putBoolean("isLoggedIn", true);
                        prefsEdit.putString("token", "TOKEN");
                        prefsEdit.putInt("userID",lastSavedUserId);
                        prefsEdit.commit();
                        Log.e("control",sharedPref.getBoolean("isLoggedIn", false)+"+++"+sharedPref.getString("token",""));


                    }
                Log.e("control",txt+"_-_-_");
            }
        });
        builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

       mAlarmDialog =builder.create();
    }
}
