package com.example.toothcareapp.view.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import com.example.toothcareapp.R;
import com.example.toothcareapp.model.dao.TimerModel;
import com.example.toothcareapp.presenter.classes.P_Timer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class F_Timer extends Fragment {
    Context mContext;
    P_Timer pTimer=new P_Timer();
    Button btnStart,btnFinish,btnReset;
    String starDate;
    String finishDate;
    DateFormat df;
    DateFormat dfTime;
    String startTime;
    String finishTime;
    Chronometer mchronometer;
    Animation atg,btgone,btgtwo,roundingalone;
    ImageView imgBgCircle,imgOk;

    public F_Timer(Context mContext){
        this.mContext=mContext;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view =  inflater.inflate(R.layout.fragment_f__timer, container, false);

        btnStart=view.findViewById(R.id.btn_start);
        btnFinish=view.findViewById(R.id.btn_finish);
        btnReset=view.findViewById(R.id.btn_reset);
        mchronometer=view.findViewById(R.id.mchronometer);
        atg= AnimationUtils.loadAnimation(getActivity(),R.anim.atg);
        btgone= AnimationUtils.loadAnimation(getActivity(),R.anim.btgone);
        btgtwo= AnimationUtils.loadAnimation(getActivity(),R.anim.btgtwo);
        roundingalone= AnimationUtils.loadAnimation(getActivity(),R.anim.roundingalone);
        imgBgCircle =view.findViewById(R.id.img_bgcircle);
        imgOk=view.findViewById(R.id.img_ok);

        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnFinish.setVisibility(View.GONE);
        btnReset.setVisibility(View.GONE);

        df = new SimpleDateFormat("dd MMM yyyy");
        dfTime = new SimpleDateFormat("HH:mm:ss");

        imgBgCircle.startAnimation(btgone);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                starDate = df.format(Calendar.getInstance().getTime());
                startTime=starDate+dfTime.format(Calendar.getInstance().getTime());
                mchronometer.start();
                imgOk.startAnimation(roundingalone);
                btnFinish.setVisibility(View.VISIBLE);
                btnReset.setVisibility(View.VISIBLE);
            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  imgOk.animate().cancel();
                //  roundingalone.reset();
                roundingalone.cancel();

                if(roundingalone.hasEnded()==true){
                    imgOk.clearAnimation();
                }

                finishDate = df.format(Calendar.getInstance().getTime());
                finishTime=dfTime.format(Calendar.getInstance().getTime());
                mchronometer.stop();

                long elapsedMillis = SystemClock.elapsedRealtime() - mchronometer.getBase();
                pTimer.addTimer(new TimerModel(starDate,finishDate,startTime,finishTime,(int)((elapsedMillis-1)*0.001)));

                for (int i = 0; i <pTimer.getTimer().size(); i++) {
                    Log.e("control",pTimer.getTimer().get(i).getStartTime()+"--"+pTimer.getTimer().get(i).getFinishTime()+"--"+(int)((elapsedMillis-1)*0.001));
                }


                //    imgOk.clearAnimation();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                starDate = df.format(Calendar.getInstance().getTime());
                mchronometer.setBase(SystemClock.elapsedRealtime());
                mchronometer.stop();
                imgOk.clearAnimation();
            }
        });
    }
}
