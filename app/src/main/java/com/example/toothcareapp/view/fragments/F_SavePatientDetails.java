package com.example.toothcareapp.view.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

import com.example.toothcareapp.R;
import com.example.toothcareapp.common.Consts;
import com.example.toothcareapp.model.dao.UserDetailsModel;
import com.example.toothcareapp.presenter.classes.P_Timer;
import com.example.toothcareapp.presenter.classes.P_UserDetails;
import com.example.toothcareapp.presenter.classes.P_UserLoginInfo;
import com.example.toothcareapp.view.activities.UserDetails;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class F_SavePatientDetails extends Fragment implements View.OnClickListener {
    Context mContext;
    private Button btnTimer, btnUserDetails,btnSave;
    private CheckBox cbxBrushTooth, cbxInterdentalBrush, cbxDentalFloss, cbxGargle;
    private boolean brushTooth, interdentalBrush, dentalFloss, gargle;
    private P_Timer p_timer=new P_Timer();
    private P_UserDetails p_userDetails=new P_UserDetails();
    private P_UserLoginInfo p_userLoginInfo=new P_UserLoginInfo();
    private String part;
    String selectedPart;
    Calendar calendar=Calendar.getInstance();

    @SuppressLint("ValidFragment")
    public F_SavePatientDetails(Context mContext) {
        this.mContext = mContext;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_f__save_patient_details, container, false);

        btnTimer = view.findViewById(R.id.btn_timer);
        btnUserDetails = view.findViewById(R.id.btn_usert_details);
        btnSave=view.findViewById(R.id.btn_save);

        cbxBrushTooth = view.findViewById(R.id.cbx_brush_tooth);
        cbxDentalFloss = view.findViewById(R.id.cbx_dental_floss_m);
        cbxGargle = view.findViewById(R.id.cbx_gargle_m);
        cbxInterdentalBrush = view.findViewById(R.id.cbx_interdental_brush_m);

        cbxBrushTooth.setOnClickListener(this);
        cbxDentalFloss.setOnClickListener(this);
        cbxGargle.setOnClickListener(this);
        cbxInterdentalBrush.setOnClickListener(this);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new F_Timer(mContext);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_frame_layout, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
            }
        });

        btnUserDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UserDetails.class);
                startActivity(intent);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            int count=0;
            @Override
            public void onClick(View v) {

                int timerResult = 0;
                int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                    if(hour>= Consts.MORNING_START && hour<=Consts.MORNING_END){
                        part="morning";
                    }else if(hour>=Consts.NOON_START && hour<=Consts.NON_END){
                        part="noon";
                    }else if(hour>=Consts.NIGHT_START && hour<=Consts.NIGHT_END){
                        part="night";
                    }else{
                        if(count==0 ){
                            partSelectDialog();
                            count++;
                        }
                        if(selectedPart !=null ){
                            part=selectedPart;
                        }

                    }
                if(p_timer.getTimer().size()!=0){
                    timerResult=p_timer.getTimer().get(p_timer.getTimer().size()-1).getElapsedTime();
                }
                int userId=p_userLoginInfo.getAllUser().get(p_userLoginInfo.getAllUser().size()-1).getId();

                Date date = new Date();

                Date date_ = Calendar.getInstance().getTime();
                SimpleDateFormat mformatter = new SimpleDateFormat("dd/MM/yyyy");
                String today = mformatter.format(date_);


                Date mdate = new Date(); // this object contains the current date value
                SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                Log.e("control","!!!"+formatter.format(mdate));
                String time=formatter.format(mdate);
                if(part!=null) {
                    Log.e("control","part="+ part +"  "+timerResult);
                    p_userDetails.addUserDetails(new UserDetailsModel(brushTooth, interdentalBrush, dentalFloss, gargle,
                            timerResult, userId,time, today, part.toLowerCase(),false));
                }
                Log.e("control",p_userDetails.userDetailsList().size()+"???"+today);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cbx_brush_tooth:
                if (cbxBrushTooth.isChecked()) {
                   brushTooth=true;
                } else{
                   brushTooth=false;
                }
                break;
            case R.id.cbx_dental_floss_m:
                if (cbxDentalFloss.isChecked()) {
                    dentalFloss=true;
                } else{
                   dentalFloss=false;
                }
                break;
            case R.id.cbx_interdental_brush_m:
                if (cbxInterdentalBrush.isChecked()){
                  interdentalBrush=true;
                }else {
                  interdentalBrush=false;
                }
                break;
            case R.id.cbx_gargle_m:
                if (cbxGargle.isChecked()){
                  gargle=true;
                }else{
                 gargle=false;
                }
                break;
        }
    }

    public void partSelectDialog(){
        final String[] result = {null};
        final String parts[] = getResources().getStringArray(R.array.part);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.select_part);
        builder.setItems(R.array.part, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedPart =parts[which];
                dialog.dismiss();

                AlertDialog.Builder builder1=new AlertDialog.Builder(getActivity());
                builder1.setTitle("!!!");
                builder1.setMessage(R.string.save_again);
                builder1.setNegativeButton(R.string.cancel, null);
                builder1.show();
            }
        });
        builder.setNegativeButton(R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}