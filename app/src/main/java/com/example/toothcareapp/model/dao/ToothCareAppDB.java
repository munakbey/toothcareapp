package com.example.toothcareapp.model.dao;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {UserLoginModel.class,AlarmModel.class,TimerModel.class,UserDetailsModel.class},
        version = 3 ,exportSchema = false)
public abstract class ToothCareAppDB extends RoomDatabase {

    private static ToothCareAppDB instance;

    public static synchronized ToothCareAppDB getInstance(Context context){
        if(instance==null){
            instance= Room.databaseBuilder(context,
                    ToothCareAppDB.class,"tooth_care_app")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract UserLoginInfoDao userInfoDao();
    public abstract  AlarmDao alarmDao();
    public  abstract  TimerDao timerDao();
    public abstract UserDetailsDao userDetailsDao();
}
