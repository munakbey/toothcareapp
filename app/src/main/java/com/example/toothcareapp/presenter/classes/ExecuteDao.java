package com.example.toothcareapp.presenter.classes;

import android.content.Context;
import android.os.AsyncTask;
import static com.example.toothcareapp.common.MyApp.getAppContext;
import com.example.toothcareapp.model.dao.AlarmDao;
import com.example.toothcareapp.model.dao.AlarmModel;
import com.example.toothcareapp.model.dao.TimerDao;
import com.example.toothcareapp.model.dao.TimerModel;
import com.example.toothcareapp.model.dao.ToothCareAppDB;
import com.example.toothcareapp.model.dao.UserLoginInfoDao;
import com.example.toothcareapp.model.dao.UserLoginModel;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ExecuteDao {

    private UserLoginInfoDao userLoginInfoDao;
    private AlarmDao alarmDao;
    private TimerDao timerDao;
    private Context mContext;

    public ExecuteDao() {
        mContext =  getAppContext();
        userLoginInfoDao = ToothCareAppDB.getInstance(mContext).userInfoDao();
        alarmDao=ToothCareAppDB.getInstance(mContext).alarmDao();
        timerDao=ToothCareAppDB.getInstance(mContext).timerDao();
    }

    public List<UserLoginModel> listUser(String username) {
        List<UserLoginModel> list=null;
        try {
            list = new GetUserInfoAsyncTask().execute(username).get(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<UserLoginModel> listUserByTpe(String userType) {
        List<UserLoginModel> list=null;
        try {
            list = new GetUserByTypeAsyncTask().execute(userType).get(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<UserLoginModel> listAllUser() {
        List<UserLoginModel> list=null;
        try {
            list = new GetAllUserInfoAsyncTask().execute().get(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void addUserInfo(UserLoginModel userLoginModel) {
        try {
            new AddUserInfoAsyncTask().execute(userLoginModel).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class GetUserByTypeAsyncTask extends AsyncTask<String, Void, List<UserLoginModel>> {
        @Override
        protected List<UserLoginModel> doInBackground(String... strings) {
            return userLoginInfoDao.getUserByType(strings[0]);
        }
    }

    private class AddUserInfoAsyncTask extends AsyncTask<UserLoginModel, Void, UserLoginModel> {
        @Override
        protected UserLoginModel doInBackground(UserLoginModel... userLoginModels) {
             userLoginInfoDao.insertUser(userLoginModels[0]);
             return null;
        }
    }

    private class GetUserInfoAsyncTask extends AsyncTask<String, Void, List<UserLoginModel>> {
        @Override
        protected List<UserLoginModel> doInBackground(String... strings) {
            return userLoginInfoDao.userInfo(strings[0]);
        }
    }

    private class GetAllUserInfoAsyncTask extends AsyncTask<Void,Void,List<UserLoginModel> >{
        @Override
        protected List<UserLoginModel> doInBackground(Void... voids) {
            return userLoginInfoDao.allUserInfo();
        }
    }
 //------------------------------------------------------------------------------------------------------------------------

    public List<AlarmModel> listAlarm(/*String alarmType*/){
        List<AlarmModel> list=null;
        try {
            list = new GetAlarmInfoAsyncTask().execute(/*alarmType*/).get(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private class GetAlarmInfoAsyncTask extends AsyncTask<Void, Void, List<AlarmModel>>{
        @Override
        protected List<AlarmModel> doInBackground(Void... voids) {
            return alarmDao.alarmInfo();
        }
    }

    public void addAlarm(AlarmModel alarmModel){
        try {
           new AddAlarmAsyncTask().execute(alarmModel).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AddAlarmAsyncTask extends AsyncTask<AlarmModel,Void,AlarmModel> {
        @Override
        protected AlarmModel doInBackground(AlarmModel... alarmModels) {
            alarmDao.addAlarm(alarmModels[0]);
            return  null;
        }
    }
//-----------------------------------------------------------------------------------------------------------------------timer methods
    public List<TimerModel> listTimer() {
         List<TimerModel> list=null;
            try {
                list = new GetAllTimerAsyncTask().execute().get(5, TimeUnit.SECONDS);
            } catch (Exception e) {
                e.printStackTrace();
            }
                return list;
        }

    public void addTimer(TimerModel timerModel) {
        try {
            new AddTimerAsyncTask().execute(timerModel).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class GetAllTimerAsyncTask  extends AsyncTask<Void, Void, List<TimerModel>>{
        @Override
        protected List<TimerModel> doInBackground(Void... voids) {
            return timerDao.timerList();
        }
    }

    private class AddTimerAsyncTask  extends AsyncTask<TimerModel, Void, TimerModel> {
        @Override
        protected TimerModel doInBackground(TimerModel... timerModels) {
            timerDao.insertTimer(timerModels[0]);
            return null;
        }
    }

}
