package com.example.toothcareapp.view.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.example.toothcareapp.R;
import com.example.toothcareapp.model.dao.UserLoginModel;
import com.example.toothcareapp.presenter.classes.P_UserLoginInfo;

public class F_UserRegister  extends Fragment {
    TextView txtName,txtSurname,txtTelNo,txtPass,txtUsername;
    Button btnRegister;
    P_UserLoginInfo p_userLoginInfo=new P_UserLoginInfo();
    public Fragment mfragment;
    Context mContext;
    String apiUsernameResult="g";
    RadioButton rb1,rb2;
    @SuppressLint("ValidFragment")

    public F_UserRegister(Context mContext) {
        this.mContext = mContext;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view =  inflater.inflate(R.layout.fragment_user_register, container, false);
        txtName=view.findViewById(R.id.txt_name);
        txtSurname=view.findViewById(R.id.txt_surname);
        txtTelNo=view.findViewById(R.id.txt_tel_no);
        btnRegister=view.findViewById(R.id.btn_register);
        txtPass=view.findViewById(R.id.txt_pass);
        txtUsername=view.findViewById(R.id.txt_username);
        rb1=view.findViewById(R.id.rb1);
        rb2=view.findViewById(R.id.rb2);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("control",txtUsername.getText().toString()+"-----//*--"+apiUsernameResult);
                if(!txtUsername.getText().toString().equals(apiUsernameResult)) {//Bu kullanıcı adı daha once kullanılmamıs ise;
                    
                    String tmp = null;
                    if(rb1.isChecked()) {
                        tmp = "patient";
                    }if(rb2.isChecked()){
                        tmp="doctor";
                    }
                    
                    p_userLoginInfo.addUserInfo(new UserLoginModel(tmp, txtUsername.getText().toString(), txtName.getText().toString()
                            , txtSurname.getText().toString(), txtPass.getText().toString(), "token", txtTelNo.getText().toString()));

                    //  SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
                    mfragment = new F_UserLogin(mContext/*,MainActivity.sharedPreferences*/);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main_frame_layout, mfragment, mfragment.getClass().getSimpleName()).addToBackStack(null).commit();
                }else{
                    AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
                    builder.setTitle("!!!");
                    builder.setMessage(R.string.already_taken);
                    builder.setNegativeButton(R.string.ok, null);
                    builder.show();
                }
            }
        });
    }
}
