package com.example.toothcareapp.presenter.classes;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.toothcareapp.common.MyApp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class AlarmReceiver extends BroadcastReceiver {
    Calendar cal=Calendar.getInstance();
    Calendar cal2=Calendar.getInstance();
    Calendar cal3 = Calendar.getInstance();
    P_Alarm p_alarm=new P_Alarm();
    int index;
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Alarm Çalıyor! ", Toast.LENGTH_SHORT).show();
        Log.e("control","blablablablablabla");

        cal2.setTimeZone(TimeZone.getDefault());
        Log.e("control",cal.get(Calendar.HOUR_OF_DAY)+"#!"+cal.get(Calendar.MINUTE));

        Log.e("control",  p_alarm.getAlarm().get(0).getMilisecond() + " ###");

        for (int i = 0; i <p_alarm.getAlarm().size(); i++) {
            if(cal.get(Calendar.HOUR_OF_DAY)== p_alarm.getAlarm().get(i).getHour() &&
                    cal.get(Calendar.MINUTE)==p_alarm.getAlarm().get(i).getMinute() ){
                index = i;
                break;
            }
        }


        long mili = cal2.getTimeInMillis();

        cal3.setTimeInMillis(mili);
        Log.e("control", cal3.getTime() + " +++");

        cal2.add(Calendar.DAY_OF_WEEK,1);
        Log.e("control",cal2.getTime()+" <-----");

         cal2.set(cal2.get(Calendar.YEAR),
                cal2.get(Calendar.MONTH),
                cal2.get(Calendar.DAY_OF_MONTH),
                p_alarm.getAlarm().get(index).getHour(), p_alarm.getAlarm().get(index).getMinute(), 0);

        Intent intent_ = new Intent(MyApp.getAppContext(), AlarmReceiver.class);//id gonder
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MyApp.getAppContext(), 0, intent_, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal2.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }
}
