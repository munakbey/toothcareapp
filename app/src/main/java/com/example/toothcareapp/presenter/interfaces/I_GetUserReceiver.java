package com.example.toothcareapp.presenter.interfaces;

import com.example.toothcareapp.model.rest.pojo.MockUser;

public interface I_GetUserReceiver {
    void onListReceived(MockUser user);

}
