package com.example.toothcareapp.view.dialogs;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TimePicker;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import com.example.toothcareapp.R;
import com.example.toothcareapp.common.MyApp;
import com.example.toothcareapp.model.dao.AlarmModel;
import com.example.toothcareapp.presenter.classes.AlarmReceiver;
import com.example.toothcareapp.presenter.classes.P_Alarm;
import com.example.toothcareapp.view.fragments.F_AlarmList;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class D_SetAlarm extends AppCompatDialogFragment implements TimePicker.OnTimeChangedListener {
    Context mContext;
    P_Alarm p_alarm=new P_Alarm();
    private static int mMinute,mHour;
    Calendar cal =Calendar.getInstance();
    private List miliseconds=new ArrayList();

    public D_SetAlarm(Context mContext) {
        this.mContext=mContext;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_set_alarm, null);

        builder.setView(view)
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setAlarm();
                    }
                });
        TimePicker picker=view.findViewById(R.id.picker);
        picker.setIs24HourView(true);
        picker.setOnTimeChangedListener(this);

        return builder.create();
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);

        mMinute=minute;
        mHour=hourOfDay;
        //p_alarm.addAlarm(new AlarmModel("morning",5666));
    }

    private long getMilisecond(){
        cal.setTimeZone(TimeZone.getDefault());
        cal.set(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH),
                mHour, mMinute, 0);

        long timeMili = cal.getTimeInMillis();
        miliseconds.add(timeMili);
        return timeMili;
    }

    private void  setAlarm() {
        long timeMilis = getMilisecond();
        p_alarm.addAlarm(new AlarmModel(F_AlarmList.alarmType,timeMilis,mHour,mMinute));
        DateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Log.e("control", timeMilis + "----- " + simple.format(timeMilis)+ " / " + cal.get(Calendar.DAY_OF_WEEK) + " ###");

        for (int i = 0; i <miliseconds.size(); i++) {
            Intent intent = new Intent(MyApp.getAppContext(), AlarmReceiver.class);//id gonder
            PendingIntent pendingIntent = PendingIntent.getBroadcast(MyApp.getAppContext(), miliseconds.size(), intent, 0);
            AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, timeMilis, AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

}
