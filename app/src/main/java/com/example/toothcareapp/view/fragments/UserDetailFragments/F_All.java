package com.example.toothcareapp.view.fragments.UserDetailFragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.toothcareapp.R;
import com.example.toothcareapp.presenter.adapters.AllAdapter;
import com.example.toothcareapp.presenter.adapters.WeeklyAdapter;
import com.example.toothcareapp.presenter.classes.P_UserDetails;
import com.example.toothcareapp.presenter.classes.P_UserLoginInfo;

public class F_All extends Fragment {
    AllAdapter allAdapter;
    P_UserDetails p_userDetails=new P_UserDetails();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view =  inflater.inflate(R.layout.fragment_f__all, container, false);

        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final RecyclerView recyclerView = view.findViewById(R.id.recyclerview_all);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        allAdapter=new AllAdapter(p_userDetails.userDetailsList(),getActivity());
        recyclerView.setAdapter(allAdapter);
    }
}
