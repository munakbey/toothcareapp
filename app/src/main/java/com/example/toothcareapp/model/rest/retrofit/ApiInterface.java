package com.example.toothcareapp.model.rest.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("...")
    Call<List<String>> getUserInfo();
}
