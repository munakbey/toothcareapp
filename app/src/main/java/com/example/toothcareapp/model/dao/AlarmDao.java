package com.example.toothcareapp.model.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface AlarmDao {

    @Insert
    public void addAlarm(AlarmModel alarmModel);

    @Query("SELECT * FROM `alarm-info`/* WHERE alarm_type = :alarm_type */")
    List<AlarmModel> alarmInfo(/*String alarm_type*/);

/*    @Query("SELECT * FROM `alarm-info` WHERE alarm_type = :alarm_type")
    List<AlarmModel> alarmWithType(String alarm_type);*/

}
