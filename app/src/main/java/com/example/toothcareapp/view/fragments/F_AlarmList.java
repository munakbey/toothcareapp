package com.example.toothcareapp.view.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.toothcareapp.R;
import com.example.toothcareapp.common.Consts;
import com.example.toothcareapp.presenter.classes.P_Alarm;
import com.example.toothcareapp.view.dialogs.D_SetAlarm;

public class F_AlarmList extends Fragment {
    public Context mContext;
    private View rootView;
    private Switch switchDay,switchMorning,switchEvening;
    public  static String alarmType;
    P_Alarm p_alarm=new P_Alarm();

    @SuppressLint("ValidFragment")
    public F_AlarmList(Context mContext ) {
        this.mContext = mContext;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view =  inflater.inflate(R.layout.fragment_f__alarm_list, container, false);

        switchMorning=view.findViewById(R.id.switch_morning);
        switchDay=view.findViewById(R.id.switch_day);
        switchEvening=view.findViewById(R.id.switch_evening);

        return  view;/*rootView=inflater.inflate(R.layout.fragment_f__doctor_main,container,false);*/
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final D_SetAlarm exampleDialog = new D_SetAlarm(mContext);

        switchMorning.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(switchMorning.isChecked()){
                    Log.e("control","switchMorning");
                    alarmType= Consts.ALARM_TYPE_MORNING;
                    exampleDialog.show(getActivity().getSupportFragmentManager(), "example dialog");
                    switchMorning.setClickable(false);
                    Log.e("control",switchMorning.getText().toString()+" -*");
                //    switchMorning.setClickable(false);
                }
            }
        });

        switchDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(switchDay.isChecked()){
                    Log.e("control","switchDay");
                    alarmType=Consts.ALARM_TYPE_DAY;
                    exampleDialog.show(getActivity().getSupportFragmentManager(), "example dialog");
                //    switchDay.setClickable(false);
                }
            }
        });

        switchEvening.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(switchEvening.isChecked()){
                    Log.e("control","switchEvening");
                    alarmType=Consts.ALARM_TYPE_EVENING;
                    exampleDialog.show(getActivity().getSupportFragmentManager(), "example dialog");
                //    switchEvening.setClickable(false);
                }
            }
        });


    }
}
