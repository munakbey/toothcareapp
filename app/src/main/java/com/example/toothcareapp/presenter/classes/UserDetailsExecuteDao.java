package com.example.toothcareapp.presenter.classes;

import android.content.Context;
import android.os.AsyncTask;
import com.example.toothcareapp.model.dao.ToothCareAppDB;
import com.example.toothcareapp.model.dao.UserDetailsDao;
import com.example.toothcareapp.model.dao.UserDetailsModel;
import com.example.toothcareapp.model.dao.UserLoginModel;

import java.util.List;
import java.util.concurrent.TimeUnit;
import static com.example.toothcareapp.common.MyApp.getAppContext;

public class UserDetailsExecuteDao {

    private Context mContext;
    UserDetailsDao userDetailsDao;

    public UserDetailsExecuteDao() {
        mContext =  getAppContext();
        userDetailsDao = ToothCareAppDB.getInstance(mContext).userDetailsDao();
    }

    public void addUserDetails(UserDetailsModel userDetailsModel) {
        try {
            new AddUserDetailsAsyncTask().execute(userDetailsModel).get();
        }catch (Exception ex){ ex.printStackTrace(); }
    }

    private class AddUserDetailsAsyncTask extends AsyncTask<UserDetailsModel,Void,UserDetailsModel> {
        @Override
        protected UserDetailsModel doInBackground(UserDetailsModel... userDetailsModels) {
            userDetailsDao.insertUserDetails(userDetailsModels[0]);
            return null;
        }
    }

    public List<UserDetailsModel> listUserDetails() {
        List<UserDetailsModel> list = null;
        try {
            list = new ListUserDetailsAsyncTask().execute().get(5, TimeUnit.SECONDS);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
            return list;
    }

    private class ListUserDetailsAsyncTask extends AsyncTask<Void,Void,List<UserDetailsModel>>{
        @Override
        protected List<UserDetailsModel> doInBackground(Void... voids) {
            return userDetailsDao.userDetailsList();
        }
    }

    public List<UserDetailsModel> listUserDetailsById(int id) {
        List<UserDetailsModel> list=null;
        try {
            list = new GetDetailsByIdTypeAsyncTask().execute(id).get(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private class GetDetailsByIdTypeAsyncTask extends AsyncTask<Integer, Void, List<UserDetailsModel>> {
        @Override
        protected List<UserDetailsModel> doInBackground(Integer... integers) {
            return userDetailsDao.userDetailsListById(integers[0]);
        }
    }
}
