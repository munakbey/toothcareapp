package com.example.toothcareapp.presenter.classes;

import com.example.toothcareapp.common.MyApp;
import com.example.toothcareapp.model.dao.TimerModel;
import com.example.toothcareapp.model.dao.UserLoginModel;

import java.util.List;

public class P_Timer {
    public List<TimerModel> getTimer( ){
        return MyApp.executer().listTimer();
    }

    public  void addTimer(TimerModel timerModel){
        MyApp.executer().addTimer(timerModel);
    }
}
