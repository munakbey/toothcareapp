package com.example.toothcareapp.model.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.toothcareapp.view.activities.UserDetails;

import java.util.List;

@Dao
public interface UserDetailsDao {
    @Insert
    public void insertUserDetails(UserDetailsModel userDetailsModel);

     @Query("SELECT * from `user-details`")
     List<UserDetailsModel> userDetailsList();

    @Query("SELECT * from `user-details` where user_id=:userID")
    List<UserDetailsModel> userDetailsListById(int userID);
}
