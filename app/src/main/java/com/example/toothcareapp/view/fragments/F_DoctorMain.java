package com.example.toothcareapp.view.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.toothcareapp.R;
import com.example.toothcareapp.common.Consts;
import com.example.toothcareapp.common.Globals;
import com.example.toothcareapp.model.dao.UserLoginModel;
import com.example.toothcareapp.model.rest.pojo.MockUser;
import com.example.toothcareapp.presenter.adapters.ShowPatientAdapter;
import com.example.toothcareapp.presenter.adapters.WeeklyAdapter;
import com.example.toothcareapp.presenter.classes.P_UserLoginInfo;
import com.example.toothcareapp.presenter.interfaces.I_GetUserReceiver;

import static com.example.toothcareapp.presenter.classes.RestExecutor.getRestExecutor;

public class F_DoctorMain extends Fragment implements I_GetUserReceiver {

    public Context mContext;
    private View rootView;
    ShowPatientAdapter showPatientAdapter;
    P_UserLoginInfo p_userLoginInfo=new P_UserLoginInfo();

    @SuppressLint("ValidFragment")
    public F_DoctorMain(Context mContext ) {
        this.mContext = mContext;
    }
    public F_DoctorMain(  ) {
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return rootView=inflater.inflate(R.layout.fragment_f__doctor_main,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
/*
        p_userLoginInfo.addUserInfo(new UserLoginModel("patient", "p", "p"
                , "p", "p", "token", "p"));
*/
        p_userLoginInfo.addUserInfo(new UserLoginModel("patient", "qqxx","qqxx"
                , "qqxx", "qqx", "token", "xxqq"));

        final RecyclerView recyclerView = view.findViewById(R.id.recyclerview_show_patient);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        showPatientAdapter=new ShowPatientAdapter(p_userLoginInfo.getUserByType(Consts.USER_PATIENT),getActivity()); //SERVERDAN CEKILECEK
        recyclerView.setAdapter(showPatientAdapter);

     //   Log.e("control",p_userLoginInfo.getUserByType(Consts.USER_PATIENT).get(0).getName()+"?_?");

            Log.e("control","?");
        getRestExecutor(mContext).getUserList(this);//Hekim hasta kayıtlarını görüntüleyebilmeli
    }

    @Override
    public void onListReceived(MockUser user) {
        Log.e("control","-------------------------");
    }
}
