package com.example.toothcareapp.view.fragments.UserDetailFragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.toothcareapp.R;
import com.example.toothcareapp.common.Globals;
import com.example.toothcareapp.presenter.classes.P_UserDetails;
import com.example.toothcareapp.view.activities.UserDetails;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class F_Daily extends Fragment {
    CheckBox cbxDentalFlossM,cbxDentalFlossN,cbxDentalFlossNG,cbxInterdentalIrushM,cbxInterdentalIrushN,cbxInterdentalIrushNG,
            cbxGargleM,cbxGargleN,cbxGargleNG;
    TextView txtMorning,txtNoon,txtNight,txtDate;
    P_UserDetails p_userDetails=new P_UserDetails();
    public int userID;
    SharedPreferences sharedPreferences;
/*    @SuppressLint("ValidFragment")
    public F_Daily(int userID) {
        this.userID = userID;
    }*/

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view =  inflater.inflate(R.layout.fragment_f__daily, container, false);

        cbxDentalFlossM =view.findViewById(R.id.cbx_dental_floss_m);
        cbxDentalFlossN=view.findViewById(R.id.cbx_dental_floss_n);
        cbxDentalFlossNG=view.findViewById(R.id.cbx_dental_floss_ng);

        cbxInterdentalIrushM =view.findViewById(R.id.cbx_interdental_brush_m);
        cbxInterdentalIrushN=view.findViewById(R.id.cbx_interdental_brush_n);
        cbxInterdentalIrushNG=view.findViewById(R.id.cbx_interdental_brush_ng);

        cbxGargleM =view.findViewById(R.id.cbx_gargle_m);
        cbxGargleN=view.findViewById(R.id.cbx_gargle_n);
        cbxGargleNG=view.findViewById(R.id.cbx_gargle_ng);

        txtMorning =view.findViewById(R.id.txt_morning);
        txtNoon=view.findViewById(R.id.txt_noon);
        txtNight=view.findViewById(R.id.txt_night);
        txtDate=view.findViewById(R.id.txt_date);

        cbxGargleM.setClickable(false);
        cbxInterdentalIrushM.setClickable(false);
        cbxDentalFlossM.setClickable(false);

        cbxGargleN.setClickable(false);
        cbxInterdentalIrushN.setClickable(false);
        cbxDentalFlossN.setClickable(false);

        cbxGargleNG.setClickable(false);
        cbxInterdentalIrushNG.setClickable(false);
        cbxDentalFlossNG.setClickable(false);



        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String today = formatter.format(date);

        SharedPreferences mPrefs = getActivity().getSharedPreferences("mSharedPref",0);
        Log.e("control","Giris YAPİLMİS ");

        if(mPrefs.getInt("selectedUserId",-1)!=-1){
            userID= Integer.valueOf(mPrefs.getInt("selectedUserId",-1));
        } else{
            userID=Integer.valueOf(mPrefs.getInt("userID",-1));
        }

        Log.e("control","++++ "+ Globals.ISDOCTOR+" -->"+userID+" "+p_userDetails.userDetailsList().size()+" "+
                p_userDetails.getuserDetailsById(userID).size());
        for (int i = 0; i <p_userDetails.getuserDetailsById(userID).size(); i++) {
            if(p_userDetails.getuserDetailsById(userID).get(i).getDate().equals(today)) {
                txtDate.setText(p_userDetails.getuserDetailsById(userID).get(i).getDate());
                if (p_userDetails.getuserDetailsById(userID).get(i).getPart().equals("morning")) {
                    txtMorning.setSingleLine(false);
                    txtMorning.setText(getResources().getString(R.string.time)+":"+p_userDetails.getuserDetailsById(userID).get(i).getTime() + "\n" +
                            getResources().getString(R.string.second)+":"+p_userDetails.getuserDetailsById(userID).get(i).getTimer_result());
                    if (p_userDetails.getuserDetailsById(userID).get(i).isDental_floss() == true) {
                        cbxDentalFlossM.setChecked(true);
                    }
                    if (p_userDetails.getuserDetailsById(userID).get(i).isInterdental_brush() == true) {
                        cbxInterdentalIrushM.setChecked(true);
                    }
                    if (p_userDetails.getuserDetailsById(userID).get(i).isGargle() == true) {
                        cbxGargleM.setChecked(true);
                    }
                }
                if (p_userDetails.getuserDetailsById(userID).get(i).getPart().equals("noon")) {
                    Log.e("control", p_userDetails.getuserDetailsById(userID).get(i).isDental_floss() + " " + p_userDetails.getuserDetailsById(userID).get(i).getDate());
                    txtNoon.setSingleLine(false);
                    txtNoon.setText(getResources().getString(R.string.time)+":"+p_userDetails.getuserDetailsById(userID).get(i).getTime() + "\n" +
                            getResources().getString(R.string.second)+":"+p_userDetails.getuserDetailsById(userID).get(i).getTimer_result());
                    if (p_userDetails.getuserDetailsById(userID).get(i).isDental_floss() == true) {
                        cbxDentalFlossN.setChecked(true);
                    }
                    if (p_userDetails.getuserDetailsById(userID).get(i).isInterdental_brush() == true) {
                        cbxInterdentalIrushN.setChecked(true);
                    }
                    if (p_userDetails.getuserDetailsById(userID).get(i).isGargle() == true) {
                        cbxGargleN.setChecked(true);
                    }
                }
                if (p_userDetails.getuserDetailsById(userID).get(i).getPart().equals("night")) {
                    txtNight.setSingleLine(false);
                    txtNight.setText(getResources().getString(R.string.time)+":"+p_userDetails.getuserDetailsById(userID).get(i).getTime() + "\n" +
                            getResources().getString(R.string.second)+":"+p_userDetails.getuserDetailsById(userID).get(i).getTimer_result());
                    if (p_userDetails.getuserDetailsById(userID).get(i).isDental_floss() == true) {
                        cbxDentalFlossNG.setChecked(true);
                    }
                    if (p_userDetails.getuserDetailsById(userID).get(i).isInterdental_brush() == true) {
                        cbxInterdentalIrushNG.setChecked(true);
                    }
                    if (p_userDetails.getuserDetailsById(userID).get(i).isGargle() == true) {
                        cbxGargleNG.setChecked(true);
                    }
                }
            }
        }

    }
}
