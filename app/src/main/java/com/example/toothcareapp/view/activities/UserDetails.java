package com.example.toothcareapp.view.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.toothcareapp.R;
import com.example.toothcareapp.common.MyApp;
import com.example.toothcareapp.presenter.adapters.SwipeAdapter;
import com.example.toothcareapp.view.fragments.F_Timer;
import com.example.toothcareapp.view.fragments.F_UserLogin;
import com.google.android.material.tabs.TabLayout;

public class UserDetails extends AppCompatActivity {
    SwipeAdapter swipeAdapter;
    ViewPager mViewPager;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        mViewPager=findViewById(R.id.mviewPager);
        tabLayout =findViewById(R.id.tabLayout);
        setPagerAdapter();
        setTabLayout();

    }

    private void setPagerAdapter(){
        swipeAdapter = new SwipeAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(swipeAdapter);
    }

    private void setTabLayout() {
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.getTabAt(0).setText(R.string.daily);
        tabLayout.getTabAt(1).setText(R.string.weekly);
        tabLayout.getTabAt(2).setText(R.string.monthly);
        tabLayout.getTabAt(3).setText(R.string.all);
    }

}
