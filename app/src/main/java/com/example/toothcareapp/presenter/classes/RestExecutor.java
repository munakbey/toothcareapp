package com.example.toothcareapp.presenter.classes;

import android.content.Context;
import android.util.Log;

import com.example.toothcareapp.model.rest.retrofit.ApiService;
import com.example.toothcareapp.presenter.interfaces.I_GetUserReceiver;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestExecutor {
    private static Context mContext;
    private static RestExecutor restExecutor;

    public static  RestExecutor getRestExecutor(Context context) {
        if (restExecutor == null){
            restExecutor = new RestExecutor();
        }
        mContext = context;
        return restExecutor;
    }

    public void getUserList( I_GetUserReceiver getUserReceiver){
    //    final I_GetUserReceiver i_getUserReceiver = getUserReceiver;
        Call<List<String>> call = ApiService.getREtro().getUserInfo();
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                if (response.isSuccessful()) {
                    Log.d("rest_control", "onResponse: isSuccessful   " + response.body());
                }else{
                    Log.d("rest_control", "onResponse: has error   " + response.body());
                }//restin execlerini yazdın test et...
            }
            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                Log.e("rest_test", "onFailure:call "+call.request().toString() );
                Log.e("rest_test", "onFailure:t "+t.toString() );
            }
        });
    }


}
