package com.example.toothcareapp.model.dao;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "alarm-info")
public class AlarmModel {
    @PrimaryKey(autoGenerate = true)
    public int id;
    private String alarm_type;
    private long milisecond;
    private int hour;
    private  int minute;

    public AlarmModel(String alarm_type, long milisecond, int hour, int minute) {
        this.alarm_type = alarm_type;
        this.milisecond = milisecond;
        this.hour = hour;
        this.minute = minute;
    }

    public void setMilisecond(long milisecond) {
        this.milisecond = milisecond;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAlarm_type() {
        return alarm_type;
    }

    public void setAlarm_type(String alarm_type) {
        this.alarm_type = alarm_type;
    }

    public long getMilisecond() {
        return milisecond;
    }

    public void setMilisecond(int milisecond) {
        this.milisecond = milisecond;
    }
}
