package com.example.toothcareapp.model.dao;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "user-details",
        foreignKeys = @ForeignKey(entity = UserLoginModel.class,
                parentColumns = "id",
                childColumns = "user_id",
                onDelete = ForeignKey.CASCADE,
                onUpdate = ForeignKey.CASCADE),indices = {@Index("user_id")})
public class UserDetailsModel {
    @PrimaryKey(autoGenerate = true)
    public int id;
    private boolean brush_tooth;
    private boolean interdental_brush;
    private boolean dental_floss;
    private boolean gargle;
    private int timer_result;
    public int user_id;
    private String time;
    private String date;
    private String part;
    private boolean uploadedFlag;

    public UserDetailsModel(boolean brush_tooth, boolean interdental_brush, boolean dental_floss, boolean gargle, int timer_result, int user_id,
                            String time, String date, String part, boolean uploadedFlag) {
        this.brush_tooth = brush_tooth;
        this.interdental_brush = interdental_brush;
        this.dental_floss = dental_floss;
        this.gargle = gargle;
        this.timer_result = timer_result;
        this.user_id = user_id;
        this.time = time;
        this.date = date;
        this.part = part;
        this.uploadedFlag = uploadedFlag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isBrush_tooth() {
        return brush_tooth;
    }

    public void setBrush_tooth(boolean brush_tooth) {
        this.brush_tooth = brush_tooth;
    }

    public boolean isInterdental_brush() {
        return interdental_brush;
    }

    public void setInterdental_brush(boolean interdental_brush) {
        this.interdental_brush = interdental_brush;
    }

    public boolean isDental_floss() {
        return dental_floss;
    }

    public void setDental_floss(boolean dental_floss) {
        this.dental_floss = dental_floss;
    }

    public boolean isGargle() {
        return gargle;
    }

    public void setGargle(boolean gargle) {
        this.gargle = gargle;
    }

    public int getTimer_result() {
        return timer_result;
    }

    public void setTimer_result(int timer_result) {
        this.timer_result = timer_result;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isUploadedFlag() {
        return uploadedFlag;
    }

    public void setUploadedFlag(boolean uploadedFlag) {
        this.uploadedFlag = uploadedFlag;
    }
}
