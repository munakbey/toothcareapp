package com.example.toothcareapp.presenter.classes;

import com.example.toothcareapp.common.MyApp;
import com.example.toothcareapp.model.dao.UserLoginModel;
import java.util.List;

public class P_UserLoginInfo {

    public List<UserLoginModel> getUser(String username){
        return MyApp.executer().listUser(username);
    }

    public List<UserLoginModel> getAllUser(){
        return MyApp.executer().listAllUser();
    }

    public  void addUserInfo(UserLoginModel userLoginModel){
        MyApp.executer().addUserInfo(userLoginModel);
    }

    public List<UserLoginModel> getUserByType(String userType){
        return MyApp.executer().listUserByTpe(userType);
    }

}
