package com.example.toothcareapp.presenter.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.toothcareapp.R;
import com.example.toothcareapp.common.Globals;
import com.example.toothcareapp.common.MyApp;
import com.example.toothcareapp.model.dao.UserDetailsModel;
import com.example.toothcareapp.presenter.classes.P_UserDetails;
import com.example.toothcareapp.presenter.classes.P_UserLoginInfo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class WeeklyAdapter extends RecyclerView.Adapter<WeeklyAdapter.MyViewHolder> {
    public List<UserDetailsModel> listItem=new ArrayList<>();
    P_UserDetails p_userDetails=new P_UserDetails();
    P_UserLoginInfo p_userLoginInfo=new P_UserLoginInfo();
    public int userID;
    SharedPreferences sharedPreferences;
    Context mContext;

    public WeeklyAdapter(List<UserDetailsModel> listItem,Context mContext) {
        this.listItem = listItem;
        this.mContext=mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(MyApp.getAppContext()).inflate(R.layout.main_weekly, parent, false);
        return new WeeklyAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {

        Calendar c = Calendar.getInstance();

        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");       // Log.e("control",",,,,,"+df.format(c.getTime()));
        String startWeekDate=df.format(c.getTime());

        SharedPreferences mPrefs =mContext.getSharedPreferences("mSharedPref",0);
        if(mPrefs.getInt("selectedUserId",-1)!=-1){
            userID= Integer.valueOf(mPrefs.getInt("selectedUserId",-1));
        } else{
            userID=Integer.valueOf(mPrefs.getInt("userID",-1));
        }

        Log.e("control","++++ "+ Globals.ISDOCTOR+" -**->"+userID+" "+p_userDetails.userDetailsList().size()+" "+
                p_userDetails.getuserDetailsById(userID).size());

        for (int j = 0; j <p_userDetails.getuserDetailsById(userID).size(); j++) {
            for (int k = 0; k <6; k++) {
                c.add(Calendar.DATE, 1);     //           Log.e("control",startWeekDate+",,,,,"+df.format(c.getTime()));
           /*     if(p_userDetails.userDetailsList().get(j).getDate().equals(df.format(c.getTime())) ||
                        p_userDetails.userDetailsList().get(j).getDate().equals(startWeekDate)) {
*/

           Log.e("control",p_userDetails.getuserDetailsById(userID).get(i).getUser_id()+" *****");
           holder.txtDate.setText(p_userDetails.getuserDetailsById(userID).get(i).getDate());
                    if (p_userDetails.getuserDetailsById(userID).get(i).getPart().equals("morning")) {
                        holder.txtMorning.setSingleLine(false);
                        holder.txtMorning.setText(MyApp.getAppContext().getResources().getString(R.string.time)+"="+p_userDetails.getuserDetailsById(userID).get(i).getTime() + "\n" +
                                MyApp.getAppContext().getResources().getString(R.string.second)+"="+p_userDetails.getuserDetailsById(userID).get(i).getTimer_result());
                        if (p_userDetails.getuserDetailsById(userID).get(i).isDental_floss() == true) {
                            holder.cbxDentalFlossM.setChecked(true);
                        }
                        if (p_userDetails.userDetailsList().get(i).isInterdental_brush() == true) {
                            holder.cbxInterdentalIrushM.setChecked(true);
                        }
                        if (p_userDetails.userDetailsList().get(i).isGargle() == true) {
                            holder.cbxGargleM.setChecked(true);
                        }
                    }
                    if (p_userDetails.getuserDetailsById(userID).get(i).getPart().equals("noon")) {
                        Log.e("control", p_userDetails.getuserDetailsById(userID).get(i).isDental_floss() + " " + p_userDetails.getuserDetailsById(userID).get(i).getDate());
                        holder.txtNoon.setSingleLine(false);
                        holder.txtNoon.setText(MyApp.getAppContext().getResources().getString(R.string.time)+":"+p_userDetails.getuserDetailsById(userID).get(i).getTime() + "\n" +
                                MyApp.getAppContext().getResources().getString(R.string.second)+":"+p_userDetails.getuserDetailsById(userID).get(i).getTimer_result());
                        if (p_userDetails.getuserDetailsById(userID).get(i).isDental_floss() == true) {
                            holder.cbxDentalFlossN.setChecked(true);
                        }
                        if (p_userDetails.getuserDetailsById(userID).get(i).isInterdental_brush() == true) {
                            holder.cbxInterdentalIrushN.setChecked(true);
                        }
                        if (p_userDetails.getuserDetailsById(userID).get(i).isGargle() == true) {
                            holder.cbxGargleN.setChecked(true);
                        }
                    }
                    if (p_userDetails.getuserDetailsById(userID).get(i).getPart().equals("night")) {
                        holder.txtNight.setSingleLine(false);
                        holder.txtNight.setText(MyApp.getAppContext().getResources().getString(R.string.time)+":"+p_userDetails.userDetailsList().get(i).getTime() + "\n" +
                                MyApp.getAppContext().getResources().getString(R.string.second)+":"+p_userDetails.userDetailsList().get(i).getTimer_result());
                        if (p_userDetails.userDetailsList().get(i).isDental_floss() == true) {
                            holder.cbxDentalFlossNG.setChecked(true);
                        }
                        if (p_userDetails.userDetailsList().get(i).isInterdental_brush() == true) {
                            holder.cbxInterdentalIrushNG.setChecked(true);
                        }
                        if (p_userDetails.userDetailsList().get(i).isGargle() == true) {
                            holder.cbxGargleNG.setChecked(true);
                        }
                    }

              //  }

            }
        }

    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CheckBox cbxDentalFlossM,cbxDentalFlossN,cbxDentalFlossNG,cbxInterdentalIrushM,cbxInterdentalIrushN,cbxInterdentalIrushNG,
                cbxGargleM,cbxGargleN,cbxGargleNG;
        TextView txtMorning,txtNoon,txtNight,txtDate;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtDate=itemView.findViewById(R.id.txt_date);

            cbxDentalFlossM =itemView.findViewById(R.id.cbx_dental_floss_m);
            cbxDentalFlossN=itemView.findViewById(R.id.cbx_dental_floss_n);
            cbxDentalFlossNG=itemView.findViewById(R.id.cbx_dental_floss_ng);

            cbxInterdentalIrushM =itemView.findViewById(R.id.cbx_interdental_brush_m);
            cbxInterdentalIrushN=itemView.findViewById(R.id.cbx_interdental_brush_n);
            cbxInterdentalIrushNG=itemView.findViewById(R.id.cbx_interdental_brush_ng);

            cbxGargleM =itemView.findViewById(R.id.cbx_gargle_m);
            cbxGargleN=itemView.findViewById(R.id.cbx_gargle_n);
            cbxGargleNG=itemView.findViewById(R.id.cbx_gargle_ng);

            txtMorning =itemView.findViewById(R.id.txt_morning);
            txtNoon=itemView.findViewById(R.id.txt_noon);
            txtNight=itemView.findViewById(R.id.txt_night);

            cbxGargleM.setClickable(false);
            cbxInterdentalIrushM.setClickable(false);
            cbxDentalFlossM.setClickable(false);

            cbxGargleN.setClickable(false);
            cbxInterdentalIrushN.setClickable(false);
            cbxDentalFlossN.setClickable(false);

            cbxGargleNG.setClickable(false);
            cbxInterdentalIrushNG.setClickable(false);
            cbxDentalFlossNG.setClickable(false);
        }
    }

}
