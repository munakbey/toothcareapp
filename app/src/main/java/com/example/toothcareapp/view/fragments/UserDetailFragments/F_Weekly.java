package com.example.toothcareapp.view.fragments.UserDetailFragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.toothcareapp.R;
import com.example.toothcareapp.presenter.adapters.WeeklyAdapter;
import com.example.toothcareapp.presenter.classes.P_UserDetails;

public class F_Weekly extends Fragment {

    WeeklyAdapter weeklyAdapter;
    P_UserDetails p_userDetails=new P_UserDetails();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view =  inflater.inflate(R.layout.fragment_f__weekly, container, false);

      /*  txtWeekOfMonth=view.findViewById(R.id.txt_week_of_month);
        txtDentalFloss=view.findViewById(R.id.txt_dental_floss_w);
        txtGargle=view.findViewById(R.id.txt_gargle_w);
        txtInterdentalBrush=view.findViewById(R.id.txt_interdental_brush_w);
*/
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final RecyclerView recyclerView = view.findViewById(R.id.recyclerview_w);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        weeklyAdapter=new WeeklyAdapter(p_userDetails.userDetailsList(),getActivity());
        recyclerView.setAdapter(weeklyAdapter);


  /*      txtWeekOfMonth.setText(x+" week of month");
        txtDentalFloss.setText("Api result");
        txtInterdentalBrush.setText("Api result");
        txtGargle.setText("Api result");*/
    }
}
