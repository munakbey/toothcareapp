package com.example.toothcareapp.presenter.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.example.toothcareapp.R;
import com.example.toothcareapp.common.MyApp;
import com.example.toothcareapp.model.dao.UserLoginModel;
import com.example.toothcareapp.presenter.classes.P_UserLoginInfo;
import com.example.toothcareapp.view.fragments.UserDetailFragments.F_Daily;
import java.util.ArrayList;
import java.util.List;

public class ShowPatientAdapter extends RecyclerView.Adapter<ShowPatientAdapter.MyViewHolder> {
    public List<UserLoginModel> listItem=new ArrayList<>();
    P_UserLoginInfo p_userLoginInfo=new P_UserLoginInfo();
    Fragment mfragment;
    Context mContext;
    SharedPreferences sharedPreferences;

    public ShowPatientAdapter(List<UserLoginModel> listItem,Context mContext) {
        this.listItem = listItem;
        this.mContext=mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(MyApp.getAppContext()).inflate(R.layout.main_show_patient, parent, false);
        return new ShowPatientAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int i) {//  KULLANICILAR SERVERDAN CEKILECEK...
        Log.e("control",listItem.get(i).getUserType());
        holder.txtName.setText(p_userLoginInfo.getAllUser().get(i).getName());
        holder.txtSurname.setText(p_userLoginInfo.getAllUser().get(i).getSurname());
        holder.txtUsername.setText(p_userLoginInfo.getAllUser().get(i).getUsername());
        holder.mlinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sharedPreferences = mContext.getSharedPreferences("mSharedPref",0);
                SharedPreferences.Editor prefsEdit = sharedPreferences.edit();
                prefsEdit.putInt("selectedUserId", listItem.get(i).getId());
                prefsEdit.commit();

                Log.e("control",listItem.get(i).id+" && "+sharedPreferences.getInt("selectedUserId",-1));
                mfragment = new F_Daily();
                ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_frame_layout, mfragment, mfragment.getClass().getSimpleName()).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

     class MyViewHolder extends  RecyclerView.ViewHolder{
        TextView txtName,txtSurname,txtUsername;
        LinearLayout mlinearLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName=itemView.findViewById(R.id.txt_name_sp);
            txtSurname=itemView.findViewById(R.id.txt_surname_sp);
            txtUsername=itemView.findViewById(R.id.txt_username_sp);
            mlinearLayout=itemView.findViewById(R.id.mLinearLayout);
        }
    }
}
