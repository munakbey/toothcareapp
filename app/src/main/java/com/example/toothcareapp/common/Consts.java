package com.example.toothcareapp.common;

public class Consts {
    public static String USER_PATIENT="patient";
    public static String USER_DOCTOR="doctor";
    public static String ALARM_TYPE_MORNING="morning";
    public static String ALARM_TYPE_DAY="day";
    public static String ALARM_TYPE_EVENING="evening";

    public static int MORNING_START=6;
    public static int MORNING_END=10;
    public static int NOON_START=12;
    public static int NON_END=15;
    public static int NIGHT_START=20;
    public static int NIGHT_END=24;



}
