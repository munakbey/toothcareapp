package com.example.toothcareapp.presenter.classes;

import com.example.toothcareapp.common.MyApp;
import com.example.toothcareapp.model.dao.AlarmModel;

import java.util.List;

public class P_Alarm {
    public List<AlarmModel> getAlarm(/*String alarmType*/){
        return MyApp.executer().listAlarm(/*alarmType*/);
    }

/*    public List<AlarmModel> getAlarmWithType(String alarmType){
        return MyApp.executer().listAlarmWithType(alarmType);
    }*/

    public void addAlarm(AlarmModel alarmModel){
         MyApp.executer().addAlarm(alarmModel);
    }
}
