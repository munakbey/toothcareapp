package com.example.toothcareapp.model.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserLoginInfoDao {

    @Query("SELECT * FROM `user-info` WHERE username = :username ")
    List<UserLoginModel> userInfo(String username);

    @Query("SELECT * FROM `user-info` ")
    List<UserLoginModel> allUserInfo();

    @Insert()
    void insertUser(UserLoginModel userLoginModel);

    @Query("SELECT * FROM `user-info` WHERE userType = :userType ")
    List<UserLoginModel> getUserByType(String userType);

}
