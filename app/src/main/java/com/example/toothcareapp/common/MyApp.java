package com.example.toothcareapp.common;

import android.app.Application;
import android.content.Context;
import com.example.toothcareapp.presenter.classes.ExecuteDao;
import com.example.toothcareapp.presenter.classes.UserDetailsExecuteDao;

public class MyApp  extends Application {
    private static ExecuteDao executor;
    private  static  UserDetailsExecuteDao userDetailsExecuter;
    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
    }
    public static Context getAppContext() {
        return MyApp.appContext;
    }

    public static ExecuteDao executer(){
        if(executor==null){
            executor=new ExecuteDao();
        }
        return  executor;
    }
    public static UserDetailsExecuteDao userDetailsExecute(){
        if(userDetailsExecuter==null){
            userDetailsExecuter=new UserDetailsExecuteDao();
        }
        return userDetailsExecuter;
    }

}
