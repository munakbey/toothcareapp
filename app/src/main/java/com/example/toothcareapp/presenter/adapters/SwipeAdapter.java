package com.example.toothcareapp.presenter.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.example.toothcareapp.view.fragments.F_DoctorMain;
import com.example.toothcareapp.view.fragments.F_PatientMain;
import com.example.toothcareapp.view.fragments.UserDetailFragments.F_All;
import com.example.toothcareapp.view.fragments.UserDetailFragments.F_Daily;
import com.example.toothcareapp.view.fragments.UserDetailFragments.F_Monthly;
import com.example.toothcareapp.view.fragments.UserDetailFragments.F_Weekly;

public class SwipeAdapter extends FragmentStatePagerAdapter {
    public SwipeAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new F_Daily();
            case 1:
                return new F_Weekly();
            case 2:
                return new F_Monthly();
            case 3:
                return new F_All();

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
