package com.example.toothcareapp.view.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.toothcareapp.R;
import com.example.toothcareapp.common.MyApp;
import com.example.toothcareapp.model.rest.pojo.MockUser;
import com.example.toothcareapp.presenter.classes.P_Alarm;
import com.example.toothcareapp.presenter.interfaces.I_GetUserReceiver;

import static com.example.toothcareapp.presenter.classes.RestExecutor.getRestExecutor;

public class F_PatientMain extends Fragment implements I_GetUserReceiver {
    private Context mContext;
    private View rootView;
    private Button btnSetAlarm,btnInfo;

    @SuppressLint("ValidFragment")
    public F_PatientMain(Context mContext ) {
        this.mContext = mContext;
    }
    public F_PatientMain(  ) {
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view =  inflater.inflate(R.layout.fragment_f__patient, container, false);

        btnSetAlarm=view.findViewById(R.id.btn_setAlarm);
        btnInfo=view.findViewById(R.id.btn_info);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnSetAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new F_AlarmList(mContext);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_frame_layout, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
            }
        });
        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new F_SavePatientDetails(MyApp.getAppContext());//!!!!!
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_frame_layout, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
            }
        });


    }

    @Override
    public void onListReceived(MockUser user) {
        Log.e("control","****");
    }
}