package com.example.toothcareapp.model.rest.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    private static String BaseURI = "http://94.101.87.88:5017/";
    private static ApiInterface apiInterface;
    public static ApiInterface getREtro() {
        if (apiInterface == null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BaseURI)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            apiInterface = retrofit.create(ApiInterface.class);
        }
        return apiInterface;
    }
}
