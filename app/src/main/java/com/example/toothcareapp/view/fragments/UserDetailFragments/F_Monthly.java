package com.example.toothcareapp.view.fragments.UserDetailFragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.toothcareapp.R;
import com.example.toothcareapp.common.Globals;
import com.example.toothcareapp.common.MyApp;
import com.example.toothcareapp.presenter.classes.P_UserDetails;

import java.util.Calendar;

public class F_Monthly extends Fragment {
    DatePicker picker;
    Button btnMonthlyDetail;
    CheckBox cbxDentalFlossM,cbxDentalFlossN,cbxDentalFlossNG,cbxInterdentalIrushM,cbxInterdentalIrushN,cbxInterdentalIrushNG,
            cbxGargleM,cbxGargleN,cbxGargleNG;
    TextView txtMorning,txtNoon,txtNight;
    P_UserDetails p_userDetails=new P_UserDetails();
    public int userID;
    SharedPreferences sharedPreferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_f__monthly, container, false);
        picker = view.findViewById(R.id.datePicker1);
        btnMonthlyDetail = view.findViewById(R.id.btn_monthly_detail);
        
        
        
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View alert, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(alert, savedInstanceState);

        Log.e("control", picker.getDayOfMonth() + "/" + (picker.getMonth() + 1) + "/" + picker.getYear() + "!!!!!" + picker.isSelected());

        btnMonthlyDetail.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(R.layout.dialog_monthly);
                
                builder.setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                cbxDentalFlossM =alert.findViewById(R.id.cbx_dental_floss_m);
                cbxDentalFlossN=alert.findViewById(R.id.cbx_dental_floss_n);
                cbxDentalFlossNG=alert.findViewById(R.id.cbx_dental_floss_ng);

                cbxInterdentalIrushM =alert.findViewById(R.id.cbx_interdental_brush_m);
                cbxInterdentalIrushN=alert.findViewById(R.id.cbx_interdental_brush_n);
                cbxInterdentalIrushNG=alert.findViewById(R.id.cbx_interdental_brush_ng);

                cbxGargleM =alert.findViewById(R.id.cbx_gargle_m);
                cbxGargleN=alert.findViewById(R.id.cbx_gargle_n);
                cbxGargleNG=alert.findViewById(R.id.cbx_gargle_ng);

                txtMorning =alert.findViewById(R.id.txt_morning);
                txtNoon=alert.findViewById(R.id.txt_noon);
                txtNight=alert.findViewById(R.id.txt_night);

                cbxGargleM.setClickable(false);
                cbxInterdentalIrushM.setClickable(false);
                cbxDentalFlossM.setClickable(false);

                cbxGargleN.setClickable(false);
                cbxInterdentalIrushN.setClickable(false);
                cbxDentalFlossN.setClickable(false);

                cbxGargleNG.setClickable(false);
                cbxInterdentalIrushNG.setClickable(false);
                cbxDentalFlossNG.setClickable(false);
                String month=String.valueOf(picker.getMonth() + 1);

    if(picker.getMonth() + 1==1 || picker.getMonth() + 1==2 || picker.getMonth() + 1==3 || picker.getMonth() + 1==4 || picker.getMonth() + 1==5 ||
                    picker.getMonth() + 1==6 || picker.getMonth() + 1==7 || picker.getMonth() + 1==8 || picker.getMonth() + 1==9){
                        month="0"+month;
                    }

                String selectedDate=picker.getDayOfMonth() + "/" + month + "/" + picker.getYear();

                SharedPreferences mPrefs =getActivity().getSharedPreferences("mSharedPref",0);
                if(mPrefs.getInt("selectedUserId",-1)!=-1){//DOKTOR HASTA GORUNTULEMEK ISTEDIGINDE
                    userID= Integer.valueOf(mPrefs.getInt("selectedUserId",-1));
                } else{                                                 //HASTA GIRISI
                    userID=Integer.valueOf(mPrefs.getInt("userID",-1));
                }

                Log.e("control","++++ "+ Globals.ISDOCTOR+" -__->"+userID+" "+p_userDetails.userDetailsList().size()+" "+
                        p_userDetails.getuserDetailsById(userID).size());



                for (int i = 0; i <p_userDetails.getuserDetailsById(userID).size(); i++) {
                    Log.e("control", p_userDetails.getuserDetailsById(userID).get(i).getDate()+"-------------"+selectedDate+"--");
                    if(p_userDetails.getuserDetailsById(userID).get(i).getDate().equals(selectedDate)) {
                        if (p_userDetails.getuserDetailsById(userID).get(i).getPart().equals("morning")) {
                            txtMorning.setSingleLine(false);
                            txtMorning.setText(getResources().getString(R.string.time)+"="+p_userDetails.getuserDetailsById(userID).get(i).getTime() + "\n" +
                                    getResources().getString(R.string.second)+"="+p_userDetails.getuserDetailsById(userID).get(i).getTimer_result());
                            if (p_userDetails.getuserDetailsById(userID).get(i).isDental_floss() == true) {
                                cbxDentalFlossM.setChecked(true);
                            }
                            if (p_userDetails.getuserDetailsById(userID).get(i).isInterdental_brush() == true) {
                                cbxInterdentalIrushM.setChecked(true);
                            }
                            if (p_userDetails.getuserDetailsById(userID).get(i).isGargle() == true) {
                                cbxGargleM.setChecked(true);
                            }
                        }
                        if (p_userDetails.getuserDetailsById(userID).get(i).getPart().equals("noon")) {
                            Log.e("control", p_userDetails.getuserDetailsById(userID).get(i).isDental_floss() + " " + p_userDetails.getuserDetailsById(userID).get(i).getDate());
                            txtNoon.setSingleLine(false);
                            txtNoon.setText(getResources().getString(R.string.time)+":"+p_userDetails.getuserDetailsById(userID).get(i).getTime() + "\n" +
                                    getResources().getString(R.string.second)+":"+p_userDetails.getuserDetailsById(userID).get(i).getTimer_result());
                            if (p_userDetails.getuserDetailsById(userID).get(i).isDental_floss() == true) {
                                cbxDentalFlossN.setChecked(true);
                            }
                            if (p_userDetails.getuserDetailsById(userID).get(i).isInterdental_brush() == true) {
                                cbxInterdentalIrushN.setChecked(true);
                            }
                            if (p_userDetails.getuserDetailsById(userID).get(i).isGargle() == true) {
                                cbxGargleN.setChecked(true);
                            }
                        }
                        if (p_userDetails.getuserDetailsById(userID).get(i).getPart().equals("night")) {
                            txtNight.setSingleLine(false);
                            txtNight.setText(getResources().getString(R.string.time)+":"+p_userDetails.getuserDetailsById(userID).get(i).getTime() + "\n" +
                                    getResources().getString(R.string.second)+":"+p_userDetails.getuserDetailsById(userID).get(i).getTimer_result());
                            if (p_userDetails.getuserDetailsById(userID).get(i).isDental_floss() == true) {
                                cbxDentalFlossNG.setChecked(true);
                            }
                            if (p_userDetails.getuserDetailsById(userID).get(i).isInterdental_brush() == true) {
                                cbxInterdentalIrushNG.setChecked(true);
                            }
                            if (p_userDetails.getuserDetailsById(userID).get(i).isGargle() == true) {
                                cbxGargleNG.setChecked(true);
                            }
                        }
                    }
                }

            }
        });
    }
}