package com.example.toothcareapp.model.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;
@Dao
public interface TimerDao {
    @Insert
    void insertTimer(TimerModel timerModel);

    @Query("SELECT * FROM `timer-info`")
    List<TimerModel> timerList();
}
